﻿using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Communication;
using Common.DTO.Account;
using Microsoft.AspNetCore.Http;
using System.Threading;
using DataAsseccLayer.Context;
using Microsoft.Extensions.Logging;
using Common.Interfaces.EntityInterfaces;
using System.Security.Claims;
using System.Collections.Concurrent;
using Common.Socket.Login;
using Common.Helpers;

namespace Services.AccountService
{
    public class AccountService : IAccountServices
    {


        private readonly MSContext _MSContext;
        private readonly ILogger<AccountService> _config;
        private readonly ConcurrentDictionary<string, SocketTokenInfo> _socketTokens;

        public AccountService(MSContext MSContext, ILogger<AccountService> config, ConcurrentDictionary<string, SocketTokenInfo> socketTokens)
        {
            _MSContext = MSContext;
            _config = config;
            _socketTokens = new ConcurrentDictionary<string, SocketTokenInfo>();
        }

        public Task<Response<UserProfilInfo>> GetAccountInfo(string token)
        {
            throw new NotImplementedException();
        }

        public async Task<Response<ClaimsIdentity>> GetIdentityAsync(string login, string password)
        {
            Response<ClaimsIdentity> response = new Response<ClaimsIdentity>();
            try {

                var user = await GetUserByCredsAsync(login, password);
                if (user == null) {
                    response.Error = new Error(404, "Invalid username or password");
                    return response;
                }
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType,user.Id),
                    new Claim(ClaimsIdentity.DefaultNameClaimType,user.Role.ToString)

                };
                ClaimsIdentity claimsIdentity = new ClaimsIdentity
                    (claims, "Token ", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
                response.Data = claimsIdentity;
                return response;
            }
        }

        public Task<Response<GetTokenForAccount>> GetToken(ClaimsIdentity identity)
        {
            throw new NotImplementedException();
        }

        public async Task<IUserEntity> GetUserByCredsAsync(string login, string password)
        {
            var encrypt = CriptorHelper.Encript(password);
            var users = await _MSContext.Users.FirstOrDefaultAsync(p=>p.Email==login&&p.Password==encrypt);
            return users;
        }

        public Task<bool> IsSocketTokenExist(string token, bool isNeedAdminPermission)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces.DTO
{
   public interface IUserProfilInfo
    {
        string Id { get; set; }
        string Name { get; set; }
    }
}

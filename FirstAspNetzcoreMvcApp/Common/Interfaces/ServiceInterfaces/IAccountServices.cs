﻿using Common.Communication;
using Common.DTO.Account;
using Common.Interfaces.EntityInterfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Services.Interfaces
{
   public interface IAccountServices
    {

        Task<IUserEntity> GetUserByCredsAsync(string login, string password);
        Task<Response<ClaimsIdentity>> GetIdentityAsync(string login,string password);
        Task<Response<GetTokenForAccount>> GetToken(ClaimsIdentity identity);
        Task<bool> IsSocketTokenExist(string token,bool isNeedAdminPermission);

        Task<Response<UserProfilInfo>> GetAccountInfo(string token);
       //Inputing SignalR


    }
}

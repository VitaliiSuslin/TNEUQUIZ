﻿using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces.EntityInterfaces
{
    public interface IUserEntity
    {
        string Id { get; set; }
        string Name { get; set; }
        string Password { get; set; }
        RoleEnum? Role { get; set; }
        
    }
}

﻿using Common.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces.EntityInterfaces
{
    public interface IToken
    {
        string Id { get; set; }

        UserEntity User { get; set; }

        string UserId { get; set; }

        DateTime ExpirationDate { get; set; }
    }
}

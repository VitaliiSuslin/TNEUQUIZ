﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces.EntityInterfaces
{
    public interface IGameEntity
    {
        long Id { get; set; }
        string Name { get; set; }
        DateTime DateTimeStart { get; set; }
        DateTime DateTimeFinish { get; set; }

        int Rezult { get; set; }
        ITestEntity testEntity { get; set; }
    }
}

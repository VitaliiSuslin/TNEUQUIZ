﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces.EntityInterfaces
{
    public interface IQuestionEntity
    {
        int Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        ICollection<IAnswerEntity> AnswerEntity { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces.EntityInterfaces
{
    public interface ITestEntity
    {
        int Id { get; set; }
        string Name { get; set; }
        ICollection<IQuestionEntity> QuestionEntity { get; set; }
    }
}

﻿using Common.Interfaces.EntityInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO.User
{
    class UserDTO
    {
       
        public string Id { get; set; }
        public string Name { get; set; }
        public UserDTO(IUserEntity user)
        {
            Id = user.Id;
            Name = user.Name;
        }
    }
}

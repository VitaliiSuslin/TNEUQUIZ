﻿using Common.Interfaces.DTO;
using Common.Interfaces.EntityInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO.Account
{
   public class UserProfilInfo : IUserProfilInfo
    {
        [Required]
        public string Name { get; set; }
        public string Id { get; set; }

        public UserProfilInfo() { }
        public UserProfilInfo(IUserEntity userEntity) {
            Name = userEntity.Name;
        }
    }
}

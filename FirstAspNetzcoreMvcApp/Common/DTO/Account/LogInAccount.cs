﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO.Account
{
   public class LogInAccount
    {
        [Required]
        public string LogIn { get; set; }
        [Required]
        [MinLength(6)]
        public string Password { get; set; }
    }
}

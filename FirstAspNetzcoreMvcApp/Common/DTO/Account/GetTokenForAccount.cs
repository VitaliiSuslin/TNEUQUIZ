﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO.Account
{
   public class GetTokenForAccount
    {
        public string Token { get; set; }
        public string Role { get; set; }
        public string UserId { get; set; }
    }
}

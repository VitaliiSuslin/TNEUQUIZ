﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO.Account
{
   public class LogOut
    {
        [Required]
        public string UserId { get; set; }
        public string Message { get; set; }
    }
}

﻿using Common.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO.Test
{
    public class ChangeTest
    {
        [Required]
        public int Id { get; set; }
      
       
        public string Name { get; set; }
       
    
        public string Description { get; set; }
  
        public ICollection<QuestionEntity> Question { get; set; }
    }
}

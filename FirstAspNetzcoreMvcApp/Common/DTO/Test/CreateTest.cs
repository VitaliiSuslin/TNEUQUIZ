﻿using Common.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO.Test
{
   public class CreateTest
    {
        [Required]
        [MinLength(6)]
        public string Name { get; set; }
        [Required]
        [MinLength(6)]
        public string Description { get; set; }
        [Required]
        public ICollection<QuestionEntity> Question { get; set; }
    }
}

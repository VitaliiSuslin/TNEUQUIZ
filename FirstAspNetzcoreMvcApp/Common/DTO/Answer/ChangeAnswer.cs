﻿using Common.Interfaces.EntityInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO.Answer
{
    public class ChangeAnswer:IAnswerEntity
    {
        [Required]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool TakeChek { get; set; }
    }
}

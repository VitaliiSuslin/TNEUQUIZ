﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO.Answer
{
   public class CreateAnswer
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public bool TakeChek { get; set; }
    }
}

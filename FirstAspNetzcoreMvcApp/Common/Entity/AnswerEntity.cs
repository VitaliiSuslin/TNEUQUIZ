﻿using Common.Interfaces.EntityInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{
  public  class AnswerEntity : IAnswerEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool TakeChek { get; set; }

        public AnswerEntity(IAnswerEntity answerEntity) {
            Id = answerEntity.Id;
            Name = answerEntity.Name;
            TakeChek = answerEntity.TakeChek;
        }
    }
}

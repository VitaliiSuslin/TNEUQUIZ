﻿using Common.Interfaces.EntityInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{
    public class GameEntity : IGameEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Name { get; set; }
        public string UserId1 { get; set; }
        public string UserId2 { get; set; }
        public DateTime DateTimeStart { get; set; }
        public DateTime DateTimeFinish { get; set; }
        public int Rezult { get; set; }
        public ITestEntity testEntity { get; set; }

        public GameEntity(IGameEntity gameEntity) {
            Id = gameEntity.Id;
            Name = gameEntity.Name;
            DateTimeStart = gameEntity.DateTimeStart;
            DateTimeFinish = gameEntity.DateTimeFinish;
            Rezult = gameEntity.Rezult;
        }
    }
}

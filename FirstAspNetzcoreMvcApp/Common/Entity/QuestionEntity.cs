﻿using Common.Interfaces.EntityInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{
    public class QuestionEntity : IQuestionEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<IAnswerEntity> AnswerEntity { get; set; }

        public QuestionEntity(IQuestionEntity questionEntity) {
            Id = questionEntity.Id;
            Name = questionEntity.Name;
            Description = questionEntity.Description;
            //Rebuild
            AnswerEntity = questionEntity.AnswerEntity;


        }
    }
}

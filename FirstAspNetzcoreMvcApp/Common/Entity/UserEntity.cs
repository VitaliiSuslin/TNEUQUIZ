﻿using Common.Interfaces.EntityInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Enum;
using Common.DTO.Account;

namespace Common.Entity
{
    public class UserEntity : IUserEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public RoleEnum? Role { get; set; }

        public void Edit(UserProfilInfo userProfilInfo) {

            Name = userProfilInfo.Name;
        }

    }
}

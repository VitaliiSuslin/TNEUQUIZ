﻿using Common.Interfaces.EntityInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{
    public class TestEntity : ITestEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<IQuestionEntity> QuestionEntity { get; set; }

        public TestEntity(ITestEntity testEntity) {
            Id = testEntity.Id;
            Name = testEntity.Name;
            QuestionEntity = testEntity.QuestionEntity;
        }
    }
}

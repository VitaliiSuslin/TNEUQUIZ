﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;


namespace Common.Helpers
{
   public static class CriptorHelper
    {
        public static string Encript(string inputText) {
            byte[] encripted = null;
            string key = "_KASPEROK_RIGTER_24_";

            using (TripleDESCryptoServiceProvider tripelSESC= new TripleDESCryptoServiceProvider()){

                tripelSESC.Key = Encoding.ASCII.GetBytes(key);
                tripelSESC.IV = Encoding.ASCII.GetBytes("_8BYTES_");

                encripted = EncryptStringToBytes(inputText, tripelSESC.Key, tripelSESC.IV);

            }
            return Convert.ToBase64String(encripted);
        }



        public static string Decriptor(string inputText) {
            var encripted = Convert.FromBase64String(inputText);
            string roundip = null;
            string key = "_KASPEROK_RIGTER_24_";

            using (TripleDESCryptoServiceProvider tripelSESC = new TripleDESCryptoServiceProvider()) {
                tripelSESC.Key = Encoding.ASCII.GetBytes(key);
                tripelSESC.IV = Encoding.ASCII.GetBytes("_8BYTES_");

                roundip = DecryptStringFromBytes(encripted, tripelSESC.Key, tripelSESC.IV);
            }

                return roundip;
        }

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] Key, byte[] IV)
        {

            if (cipherText == null || cipherText.Length <= 0) { throw new ArgumentNullException("cipherText"); }
            if (Key == null || Key.Length <= 0) { throw new ArgumentNullException("Key"); }
            if (IV == null || IV.Length <= 0) { throw new ArgumentNullException("IV"); }

            string plaintext = null;

            using (TripleDESCryptoServiceProvider tripelSESC = new TripleDESCryptoServiceProvider())
            {
                tripelSESC.Key = Key;
                tripelSESC.IV = Key;

                ICryptoTransform cryptoTransform = tripelSESC.CreateEncryptor(tripelSESC.Key, tripelSESC.IV);

                using (MemoryStream memory = new MemoryStream())
                {
                    using (CryptoStream csDecript = new CryptoStream(memory, cryptoTransform, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecript = new StreamReader(csDecript))
                        {
                            plaintext = srDecript.ReadToEnd(); 
                        }
                     
                    }

                }

            }

            return plaintext;
        }

        static byte[] EncryptStringToBytes(string plainText,byte[] Key,byte[] IV)
        {
            if (plainText == null || plainText.Length <= 0) { throw new ArgumentNullException("plainText"); }
            if (Key == null || Key.Length <= 0) { throw new ArgumentNullException("Key"); }
            if (IV == null || IV.Length <= 0) { throw new ArgumentNullException("IV"); }


            byte[] encripted;
            using (TripleDESCryptoServiceProvider tripelSESC = new TripleDESCryptoServiceProvider())
            {
                tripelSESC.Key = Key;
                tripelSESC.IV = Key;

                ICryptoTransform cryptoTransform = tripelSESC.CreateEncryptor(tripelSESC.Key, tripelSESC.IV);

                using (MemoryStream memory=new MemoryStream()) {
                    using (CryptoStream csEncript=new CryptoStream(memory,cryptoTransform,CryptoStreamMode.Write)) {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncript)) {
                            swEncrypt.Write(plainText);
                        }
                        encripted = memory.ToArray();
                    }

                }
              
            }
            return encripted;
        }


    }
}

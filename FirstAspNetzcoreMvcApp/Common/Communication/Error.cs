﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Communication
{
    public class Error
    {

        public Error() { }

        public Error(string descriprion) {
            ErrorDescription = descriprion;
            ErrorCode = 500;
        }

        public Error(int errorCode,string descriprion)
        {
            ErrorDescription = descriprion;
            ErrorCode = errorCode;
        }

        public int ErrorCode { get; private set; }

        public string ErrorDescription { get; set; }

        public override string ToString()
        {
            return $"Status code:{ErrorCode},\n Description:{ErrorDescription}";
        }
    }
}

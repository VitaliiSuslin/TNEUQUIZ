﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Communication
{
    public class DoubleResponse<T,DT>:Response<T>
    {
        public DT AdditionalData { get; set; }
    }
}

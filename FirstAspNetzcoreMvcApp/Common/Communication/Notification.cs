﻿using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Communication
{
   public class Notification <T>
    {
        public NotificationAction Action { get; set; }
        public T Data { get; set; }
    }
}

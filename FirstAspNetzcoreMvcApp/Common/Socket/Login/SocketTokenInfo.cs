﻿using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Socket.Login
{
   public class SocketTokenInfo
    {
        public DateTime Expirate { get; set; }
        public RoleEnum? Role { get; set; }
    }
}

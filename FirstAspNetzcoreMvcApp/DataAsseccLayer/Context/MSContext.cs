﻿using Common.Entity;
using Microsoft.EntityFrameworkCore;

namespace DataAsseccLayer.Context
{
    public class MSContext:DbContext
    {
        public MSContext(DbContextOptions<MSContext> options):base(options) { }



        public DbSet<AnswerEntity> Answers { get; set; }
        public DbSet<GameEntity> Games { get; set; }
        public DbSet<QuestionEntity> Questions { get; set; }
        public DbSet<TestEntity> Tests { get; set; }
        public DbSet<TokenEntity> Tokens { get; set; }
        public DbSet<UserEntity> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
          //Inpute the modelBuilder
        }
    }
}

﻿using DataAsseccLayer.Context;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DataAsseccLayer.Migration
{
    [DbContext(typeof(MSContext))]
    partial class MSContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
              .HasAnnotation("ProductVersion", "1.1.2")
              .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);
            modelBuilder.Entity("Common.Entity.AnswerEntity",b=>
            {
                b.Property<int>("Id").ValueGeneratedOnAdd();
                b.Property<string>("Name");
                b.Property<bool>("TakeChek");

                b.HasKey("Id");
                b.ToTable("Answers");
            });

            modelBuilder.Entity("Common.Entity.GameEntity",b=>
            {
                b.Property<long>("Id").ValueGeneratedOnAdd();
                b.Property<string>("Name");
                b.Property<string>("UserId1");
                b.Property<string>("UserId2");
                b.Property<DateTime>("DateTimeStart");
                b.Property<DateTime>("DateTimeFinish");

                b.HasKey("Id");
                b.HasIndex("UserId1");
                b.HasIndex("UserId2");
                b.ToTable("Games");
            });

            modelBuilder.Entity("Common.Entity.QuestionEntity",b=>
            {
                b.Property<int>("Id").ValueGeneratedOnAdd();
                b.Property<string>("Name");
                b.Property<string>("Description");

                b.HasKey("Id");
                b.ToTable("Questions");
            });
            modelBuilder.Entity("Common.Entity.TestEntity",b=>{
                b.Property<string>("Id").ValueGeneratedOnAdd();
                b.Property<DateTime>("ExpirationDate");
                b.Property<string>("UserId");

                b.HasKey("Id");
                b.HasIndex("UserId");
                b.ToTable("Tests");
            });
            modelBuilder.Entity("Common.Entity.UserEntity",b=>{

                b.Property<string>("Id").ValueGeneratedOnAdd();
                b.Property<string>("Name");
                b.Property<string>("Password");
                b.Property<int?>("Role");

                b.HasKey("Id");
                b.ToTable("Users");
            });




        }
    }
}
